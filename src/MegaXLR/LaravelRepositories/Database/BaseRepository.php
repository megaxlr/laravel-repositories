<?php


namespace MegaXLR\LaravelRepositories\Database;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use MegaXLR\LaravelRepositories\Contracts\RepositoryInterface;
use MegaXLR\LaravelRepositories\Exceptions\RepositoryException;


/**
 * Class Repository
 * @package App\Repositories
 */
abstract class Repository implements RepositoryInterface
{

    /**
     * @var App
     */
    private $app;


    /**
     * @var Model
     */
    protected $model;


    /**
     * @var Collection
     */
    protected $criteria;


    /**
     * @var bool
     */
    protected $skipCriteria = false;


    /**
     * Repository constructor.
     * @param App $app
     * @param Collection $collection
     * @throws \Exception
     */
    public function __construct()
    {
        $this->app = app();
        $this->makeModel();
        $this->criteria = collect([]);
        $this->resetScope();
    }


    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     * @author <ted@privacyzeker.nl>
     */
    public function paginate($perPage = 15, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->paginate($perPage, $columns);
    }


    /**
     * Get the classname of the resource structure identifier.
     * @return mixed
     * @author <admin@megaxlr.net>
     * @throws \Exception
     */
    abstract function model();


    /**
     * @return Model|mixed
     * @throws \Exception
     * @author <ted@privacyzeker.nl>
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if(!$model instanceof Model) throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }


    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|Model[]|mixed
     * @author <admin@megaxlr.net>
     */
    public function all($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->get($columns);
    }


    /**
     * @param array $columns
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function first($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->first($columns);
    }


    /**
     * @param array $data
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function create(array $data)
    {
        if(method_exists($this, 'beforeCreate')) $data = $this->beforeCreate($data);
        try {
            $created = $this->model->create($data);
            return $created;
        } catch(\Exception $e) {
            Log::warning($e);
        }
    }


    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function update(array $data, $id, $attribute="id")
    {
        $model = $this->model->where($attribute, '=', $id);
        $model->update($data);
        return $model->first();
    }


    /**
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function firstOrCreate(array $attributes, array $values)
    {
        $this->applyCriteria();
        return $this->model->firstOrCreate($attributes, $values);
    }


    /**
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function updateOrCreate(array $attributes, array $values)
    {
        $this->applyCriteria();
        return $this->model->updateOrCreate($attributes, $values);
    }


    /**
     * @param $id
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }


    /**
     * @param $id
     * @param array $columns
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function find($id, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->find($id, $columns);
    }


    /**
     * Return a bare resource instance for this repository.
     * @param array $columns
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function new($columns = array())
    {
        $resource = $this->model;
        $model = new $resource;
        $model->fill($columns);
        return $model;
    }


    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->where($attribute, '=', $value)->first($columns);
    }


    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function findAllBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->model->where($attribute, '=', $value)->all($columns);
    }


    /**
     * @param bool $status
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;
        return $this;
    }


    /**
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function getCriteria()
    {
        return $this->criteria;
    }


    /**
     * @param Criteria $criteria
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function getByCriteria(Criteria $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);
        return $this;
    }


    /**
     * @param Criteria $criteria
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function criteria(Criteria $criteria)
    {
        $this->criteria->push($criteria);
        return $this;
    }


    /**
     * @return mixed
     * @author <admin@megaxlr.net>
     */
    public function applyCriteria()
    {
        if($this->skipCriteria) return $this;

        foreach($this->getCriteria() as $criteria)
            if($criteria instanceof Criteria) $this->model = $criteria->apply($this->model, $this);
    }


    /**
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function resetScope()
    {
        $this->skipCriteria(false);
        return $this;
    }


    /**
     * Eager load relations in the query
     *
     * @param $relations
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function with($relations)
    {
        $this->model = $this->model->with($relations);
        return $this;
    }


    /**
     * @param $relations
     * @return $this
     * @author <admin@megaxlr.net>
     */
    public function withCount($relations) {
        $this->model = $this->model->withCount($relations);
        return $this;
    }
}
