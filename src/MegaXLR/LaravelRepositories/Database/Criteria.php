<?php


namespace MegaXLR\LaravelRepositories\Database;

use Illuminate\Database\Eloquent\Model;

interface Criteria
{

    /**
     * @param $model
     * @param Repository $repository
     * @return mixed
     * @author <ted@privacyzeker.nl>
     */
    public function apply($model, Repository $repository);
}