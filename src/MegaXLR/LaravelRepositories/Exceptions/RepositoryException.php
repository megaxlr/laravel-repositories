<?php
namespace MegaXLR\LaravelRepositories\Exceptions;

/**
 * Class RepositoryException
 * @package MegaXLR\LaravelRepositories\Exceptions
 */
class RepositoryException extends \Exception
{

}