<?php


namespace MegaXLR\LaravelRepositories\Providers;


use MegaXLR\LaravelRepositories\Console\Commands\MakeRepositoryCommand;
use Illuminate\Support\ServiceProvider;
use MegaXLR\LaravelRepositories\Console\Commands\MakeCriteriaCommand;

class RepositoryProvider extends ServiceProvider
{
    protected $defer = true;


    /**
     * @author <admin@megaxlr.net>
     */
    public function boot()
    {
        //
    }


    /**
     * @author <ted@privacyzeker.nl>
     */
    public function register()
    {
        $this->registerCommands();
    }


    /**
     * @author <ted@privacyzeker.nl>
     */
    public function registerCommands()
    {
        $this->app->bind('command.repository.make', function(\Illuminate\Contracts\Foundation\Application $app) {
            return $app->make(MakeRepositoryCommand::class);
        });

        $this->app->bind('command.criteria.make', function(\Illuminate\Contracts\Foundation\Application $app) {
            return $app->make(MakeCriteriaCommand::class);
        });

        $this->commands(['command.repository.make', 'command.criteria.make']);
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'command.repository.make',
            'command.criteria.make'
        ];
    }
}
