<?php


namespace MegaXLR\LaravelRepositories\Console\Commands;


use Illuminate\Console\GeneratorCommand;

class MakeCriteriaCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:criteria';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Criteria class';


    /**
     * @var string
     */
    public $type = "Criteria";


    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repositories\Criteria';
    }


    /**
     * @return string
     * @author <admin@megaxlr.net>
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/criteria.stub';
    }
}